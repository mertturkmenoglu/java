public class Oda {
    private String isim;

    public Oda(String isim) {
        this.isim = isim;
    }

    public void setIsim(String isim) {
        this.isim = isim;
    }

    public String getIsim() {
        return isim;
    }
}
