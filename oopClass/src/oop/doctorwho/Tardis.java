package oop.doctorwho;

@SuppressWarnings({"WeakerAccess", "unused"})
public class Tardis {
    private TimeLord timeLord;

    public TimeLord getTimeLord() {
        return timeLord;
    }

    public void setTimeLord(TimeLord timeLord) {
        this.timeLord = timeLord;
    }
}
